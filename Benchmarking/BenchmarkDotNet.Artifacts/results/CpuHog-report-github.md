``` ini

BenchmarkDotNet=v0.10.1, OS=Windows
Processor=?, ProcessorCount=8
Frequency=3906532 Hz, Resolution=255.9815 ns, Timer=TSC
dotnet cli version=1.0.0-preview2-1-003177
  [Host]     : .NET Core 4.6.24628.01, 64bit RyuJIT
  DefaultJob : .NET Core 4.6.24628.01, 64bit RyuJIT

Allocated=7 B  

```
 Method |      Mean |    StdDev |
------- |---------- |---------- |
 BeAHog | 4.8976 ms | 0.0064 ms |
------ |---------- |
 BeAHog | 413.0391 ms | 0.8696 ms |
