﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Benchmarking
{
    public class CpuHog
    {
        [Benchmark]
        public void BeAHog()
        {
            for (var i = 0; i < 10000000; i++)
            {
                Math.Sqrt(Math.Sqrt((i + 34) * 82232373));
            }
        }
    }
}
