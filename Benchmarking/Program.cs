﻿using System;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Running;

namespace Benchmarking
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BenchmarkRunner.Run<CpuHog>(ManualConfig
                .Create(DefaultConfig.Instance)
                .With(RPlotExporter.Default)
                .With(CsvMeasurementsExporter.Default));

            Console.WriteLine("Done...");
            Console.ReadKey();
        }
    }
}
